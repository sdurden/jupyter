import pyodbc
import pandas as pd
import datetime

server = 'tcp:172.22.12.10\KDB_BI'
database = 'kddbcorporativo'
username = 'jlortiz'
password = 'Escarnobroske2K'
cnxn = pyodbc.connect('DRIVER={ODBC Driver 13 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username+';PWD='+ password)

ahora= datetime.datetime.now() + datetime.timedelta(days=-2)

AA=ahora.strftime("%Y")
MM=ahora.strftime("%m")
DD=ahora.strftime("%d")
# print(str(ahora.month))
fin = ahora + datetime.timedelta(days=-30)

aa=fin.strftime("%Y")
mm=fin.strftime("%m")
dd=fin.strftime("%d")

consulta="""
 declare @feci date
 declare @fecf date

set @fecf=dateadd(dd,-2,GETDATE())
set @feci=DATEADD(dd,-30,@fecf)

set nocount on
-- Ventas
-- delimitado por |
Select mv.[Transaction No_]                                        as 'Ticket'         ,
       mv.[Store No_]                                              as 'Tienda'         ,
	   mv.[Sales Staff]                                            as 'Vendedor'       ,
	   mv.[Date]+' '+convert(varchar, mv.[time], 8)                as 'Fecha'          ,
       mv.[Barcode No_]                                            as 'Barcode'        ,
       mv.[quantity] *-1                                           as 'Cantidad'       ,
       mv.[quantity] *-1 * mv.[Net Price]                          as 'Imp Vta/Bruta'  ,
       (mv.[quantity]*-1 * mv.[Net Price]) - (mv.[Net Amount]*-1)  as 'Imp. Descto'    ,
       (mv.[Net Amount]*-1)                                        as 'Imp Vta. Neto'  ,
       mv.[Total Rounded Amt_] * -1                                as 'Imp. Vta C/Iva'
  from [Roberts$Trans_ Sales Entry] as mv with (nolock)
 where mv.[Date] >= @feci
   and mv.[Date] <= @fecf
   AND mv.[Store No_] IN 
( 'R001', 
  'R002')

"""

result = pd.read_sql(consulta,cnxn)
#result.to_csv(r'.\Datos\Vta{}{}{}_{}{}{}.csv'.format(DD,MM,AA,dd,mm,aa),index=None,header=True,sep='|',encoding='utf-8-sig')
