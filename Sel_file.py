import tkinter
from tkinter.filedialog import askopenfile

root = tkinter.Tk()
file = askopenfile(parent=root,mode='r',title='Elije un archivo')
if file != None:
    data = file.read()
    file.close()
    print("I got %d bytes from this file." % len(data))
