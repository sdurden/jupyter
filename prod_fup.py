import pyodbc
import pandas as pd

server = 'tcp:172.22.12.10\KDB_BI'
database = 'kddbcorporativo'
username = 'jlortiz'
password = 'Escarnobroske2K'
cnxn = pyodbc.connect('DRIVER={ODBC Driver 13 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username+';PWD='+ password)
consulta=""
consulta= """set nocount on
-- Catalogo de Productos
-- delimitado por |
select cr.[Item No_]                     ,
       cr.[Cross-Reference No_]          as Barcode,
	   [Item Category Code]              ,
	   [PfsBrand]                        ,
	   [PfsSeason]                       ,
              isnull(case
                        when left(Cr.[Variant Code],3) in (select [Code]
                                                             from [PfsVert Component Base] with (Nolock)
                                                            where left(Cr.[Variant Code],3) = [Code]) then
                              case STUFF(Cr.[Variant Code], 1, 4, '')
                                   WHEN '1ECH' THEN 'ECH'
                                   WHEN '2CH'  THEN 'CH'
                                   WHEN '3M'   THEN 'M'
                                   WHEN '4G'   THEN 'G'
                                   WHEN '51EG' THEN '1EG'
                                   WHEN '62EG' THEN '2EG'
                                   WHEN '73EG' THEN '3EG'
                                   else  STUFF(Cr.[Variant Code], 1, 4, '')
                              end
                        WHEN Cr.[Variant Code] = '1ECH' THEN 'ECH'
                        WHEN Cr.[Variant Code] = '2CH'  THEN 'CH'
                        WHEN Cr.[Variant Code] = '3M'   THEN 'M'
                        WHEN Cr.[Variant Code] = '4G'   THEN 'G'
                        WHEN Cr.[Variant Code] = '51EG' THEN '1EG'
                        WHEN Cr.[Variant Code] = '62EG' THEN '2EG'
                        WHEN Cr.[Variant Code] = '73EG' THEN '3EG'
                        ELSE                                 Cr.[Variant Code]
                      END,'UNI')                                                    as 'Talla'             ,

              cASE PR.[PfsComponent 4] + PR.[PfsComponent 5]
                   WHEN '1C'   THEN  'C'
                   WHEN '2R'   THEN  'R'
                   WHEN '3L'   THEN  'L'
                   WHEN '5'    THEN  '5'
                   ELSE              PR.[PfsComponent 4] + PR.[PfsComponent 5]
              END                                                                   as 'Cpx'               ,
              Case
                   when Pr.[PfsComponent 3] =  '000'	then	'Blanco'
                   when Pr.[PfsComponent 3] =  '222'	then	'Sin Teñir'
                   when Pr.[PfsComponent 3] =  '223'	then	'Varios'
                   when Pr.[PfsComponent 3] =  '777'	then	'Combinado'
                   when Pr.[PfsComponent 3] =  '888'	then	'Fantasia'
                   when Pr.[PfsComponent 3] =  '999'	then	'Negro'
                   when Pr.[PfsComponent 3] =  'A00'	then	'Amarillo Claro'
                   when Pr.[PfsComponent 3] =  'A50'	then	'Amarillo Medio'
                   when Pr.[PfsComponent 3] =  'A90'	then	'Amarillo Obscuro'
                   when Pr.[PfsComponent 3] =  'B00'	then	'Azul Claro'
                   when Pr.[PfsComponent 3] =  'B50'	then	'Azul Medio'
                   when Pr.[PfsComponent 3] =  'B90'	then	'Azul Obscuro'
                   when Pr.[PfsComponent 3] =  'C00'	then	'Beige Claro'
                   when Pr.[PfsComponent 3] =  'C50'	then	'Beige Medio'
                   when Pr.[PfsComponent 3] =  'C90'	then	'Beige Obscuro'
                   when Pr.[PfsComponent 3] =  'D00'	then	'Café Claro'
                   when Pr.[PfsComponent 3] =  'D50'	then	'Café Medio'
                   when Pr.[PfsComponent 3] =  'D90'	then	'Café Obscuro'
                   when Pr.[PfsComponent 3] =  'E00'	then	'Gris Claro'
                   when Pr.[PfsComponent 3] =  'E50'	then	'Gris Medio'
                   when Pr.[PfsComponent 3] =  'E90'	then	'Gris Obscuro'
                   when Pr.[PfsComponent 3] =  'F00'	then	'Morado Claro'
                   when Pr.[PfsComponent 3] =  'F50'	then	'Morado Medio'
                   when Pr.[PfsComponent 3] =  'F90'	then	'Morado Obscuro'
                   when Pr.[PfsComponent 3] =  'G00'	then	'Naranja Claro'
                   when Pr.[PfsComponent 3] =  'G50'	then	'Naranja Medio'
                   when Pr.[PfsComponent 3] =  'G90'	then	'Naranja Obscuro'
                   when Pr.[PfsComponent 3] =  'I00'	then	'Rojo Claro'
                   when Pr.[PfsComponent 3] =  'I50'	then	'Rojo Medio'
                   when Pr.[PfsComponent 3] =  'I90'	then	'Rojo Obscuro'
                   when Pr.[PfsComponent 3] =  'J00'	then	'Rosa Claro'
                   when Pr.[PfsComponent 3] =  'J50'	then	'Rosa Medio'
                   when Pr.[PfsComponent 3] =  'J90'	then	'Rosa Obscuro'
                   when Pr.[PfsComponent 3] =  'K00'	then	'Verde Claro'
                   when Pr.[PfsComponent 3] =  'K50'	then	'Verde Medio'
                   when Pr.[PfsComponent 3] =  'K90'	then	'Verde Obscuro'
                   when left(cr.[Variant Code],3) in (select [Code]
                                                        from [PfsVert Component Base] with (Nolock)
                                                       where left(cr.[Variant Code],3) = [Code]) then
                         isnull((select [Description]
                                   from [PfsVert Component Base] with (Nolock)
                                  where left(cr.[Variant Code],3) = [Code]),'Combinado')
                   else                isnull(Pr.[PfsComponent 3],'Combinado')
              end                                                                                      as 'Color'        ,
			  pr.[Unit Price]                                                                          as 'Precio S/Iva' ,
			  round(pr.[Unit Price]*1.16,0)                                                            as 'Precio S/Iva'

  from [Roberts$Item Cross Reference] cr,
       [Roberts$Item] pr
 where pr.[No_] = cr.[Item No_]
   and [Item Category Code]<>''
   and [PfsBrand]<>''
   and cr.[Item No_]<>cr.[Cross-Reference No_]
order by cr.[Item No_]                   ,
       cr.[Cross-Reference No_]          ,
	   [Item Category Code]              ,
	   [PfsBrand]                        ,
	   [PfsSeason]
"""
#cnxn.setdecoding(pyodbc.SQL_CHAR, encoding='utf-8')
result = pd.read_sql(consulta,cnxn)
#result.to_csv(r'.\Datos\productos.csv',index=None,header=True,sep='|',encoding='utf-8-sig')