import pyodbc
import pandas as pd
from datetime import datetime
from tkinter import Tk
from tkinter.filedialog import askopenfile
import pyodbc

bandera=0
server = 'tcp:172.22.12.13\DASHBOARD'
database = 'dbLamberti'
username = 'srodriguez'
password = 'Zmadgfv1'
cnxn = pyodbc.connect('DRIVER={ODBC Driver 13 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username+';PWD='+ password)
cursor = cnxn.cursor()
fec = input("¿A partir de qué fecha se extraerán los datos? DD/MM/YYY : ")
consulta=""" Select distinct top 10  convert(date,[Fecha registro],03) [Fecha registro] from VentaPH1 where [Fecha registro]>= ? order by 1 desc 

"""
cursor.execute(consulta,fec)

print("Estas son las últimas fechas insertadas en la Tabla de Ventas \n")


if cursor.rowcount == 0 :
    bandera = 1
else:
    bandera = 0


while True:
    row= cursor.fetchone()

    if not row:
        break
    print(row[0])

input()

root = Tk() # we don't want a full GUI, so keep the root window from appearing
root.withdraw()
filename = askopenfile(parent=root,mode='r',title='Elije un archivo') # show an "Open" dialog box and return the path to the selected file


borrar = ["Dia",
          "Centro",
          "Clave de Articulo",
          "Codigo LA",
          "Venta Neta en UM",
          "Venta Neta s/Cupones a Rtl",
          "Total Rebajas y Descuentos"]

Tiendas = {"Tienda Interlomas" :"BMPH001",
           "Tienda Durango" : "BMPH002",
           "Tienda Querétaro" :"BMPH003",
           "Tienda Perisur" : "BMPH004",
           "Tienda Santa Fe" : "BMPH005",
           "Ventas por Internet" : "BMPH930"}


Ventas = pd.read_excel('C:/Users/Israel/Documents/python/Datos/Ventas Boggi Abril-sept.xlsx', sheet_name="Abril-Sept")
Ventas=Ventas[Ventas['Dia']>=pd.to_datetime(fec,format='%d/%m/%Y')]

if Ventas.size == 0:
    bandera = 0

selección =[]
for x in Ventas.columns:
    if x not in borrar:
        selección.append(x)

Ventas = Ventas.drop(selección,axis=1)

for r, lab in Ventas.iterrows():
    Ventas.loc[r,"Cód"]=Tiendas[lab['Centro']]
	

insert = """insert into VentaPH1 ([Fecha registro],[Cód. almacén],[Clave articulo],[Nº producto],[Cantidad],Importe,[Importe Dto. Línea] ) 
values ( ? , ? , ? , ? , ? , ? ,? )
"""
if bandera == 1:
    Ventas = Ventas[['Dia', 'Cód', 'Clave de Articulo', 'Codigo LA', 'Venta Neta en UM',
                     'Venta Neta s/Cupones a Rtl', 'Total Rebajas y Descuentos']]

    Ventas['Clave de Articulo'] = Ventas['Clave de Articulo'].astype(object)

    Ventas['Total Rebajas y Descuentos'].fillna(0, inplace=True)
    for x,y in Ventas.iterrows():
        cursor.execute(insert, y["Dia"], y["Cód"], y["Clave de Articulo"], y["Codigo LA"], y["Venta Neta en UM"], y["Venta Neta s/Cupones a Rtl"], y["Total Rebajas y Descuentos"])
        cnxn.commit()
    actualiza = """update VPH set [Tipo movimiento]='Venta',
               VPH.[Grupo Serie]=P.[Grupo serie],
			   VPH.Familia=P.Familia,
			   VPH.Color=P.Color,
			   VPH.Colección=P.Colección,
			   VPH.Talla=p.Talla,
			   VPH.Modelo=P.[Cód. producto proveedor],
			   VPH.Descripción=P.Descripción,
			   VPH.[Precio unitario]=P.[Precio venta],
			   VPH.[Precio coste]=P.[Precio coste]
                from VentaPH1 VPH join Producto P on VPH.[Nº producto]=P.Nº"""
    cursor.execute(actualiza)
    cnxn.commit()
    print("Los datos ya han sido cargados y actualizados")
else :
    print("La base ya cuenta con datos con fechas >=",fec," o el rango no existe en el archivo seleccionado")