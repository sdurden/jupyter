declare @fecha as date,
@date as date,
@añoactual as int,
@mes as int,
@tempact as nvarchar(25),
@tempant1 as nvarchar (25),
@tempant2 as nvarchar(25),
@tempant3 as nvarchar(25),
@tempost1 as nvarchar(25),
@tempost2 as nvarchar(25)

set @date=dateadd(DD,-1,GETDATE())
set @fecha=(select dateadd(dd,-DAY(@date),dateadd(mm,1,@date)))
set @añoactual=year(@fecha)
set @mes=month(@fecha)


set @tempact= concat(cast(case when @mes in(1,2,3,4,5,6,7,8,9,10) then right(@añoactual,2) else right(@añoactual,2)+1 end as nvarchar),
		cast(case when @mes  in(11,12,1) then 1
						     when  @mes in(2,3,4) then 2
							 when  @mes in(5,6,7) then 3
							 when  @mes in(8,9,10) then 4 end  as nvarchar)) 

set @tempant1=  concat(convert(nvarchar,case when @mes in(2,3,4,5,6,7,8,9,10) then right(@añoactual,2) when @mes in(1) then right(@añoactual,2)-1   else right(@añoactual,2) end),
					   convert(varchar,case when @mes  in(11,12,1) then 4
						     when  @mes in(2,3,4) then 1
							 when  @mes in(5,6,7) then 2
							 when  @mes in(8,9,10) then 3 end ))

set @tempant2=  concat(convert(nvarchar,case when @mes in(1,2,3,4) then right(@añoactual,2)-1 else right(@añoactual,2) end),
					   convert(varchar,case when @mes  in(11,12,1) then 3
						     when  @mes in(2,3,4) then 4
							 when  @mes in(5,6,7) then 1
							 when  @mes in(8,9,10) then 2 end ))
set @tempant3=  concat(convert(nvarchar,case when @mes in(1,2,3,4,5,6,7) then right(@añoactual,2)-1 else right(@añoactual,2) end),
					   convert(varchar,case when @mes  in(11,12,1) then 2
						     when  @mes in(2,3,4) then 3
							 when  @mes in(5,6,7) then 4
							 when  @mes in(8,9,10) then 1 end ))							 																	 							
set @tempost1=  concat(convert(nvarchar,case when @mes in(1,2,3,4,5,6,7) then right(@añoactual,2) else right(@añoactual,2)+1 end),
					   convert(varchar,case when @mes  in(11,12,1) then 2
						     when  @mes in(2,3,4) then 3
							 when  @mes in(5,6,7) then 4
							 when  @mes in(8,9,10) then 1 end   ))
set @tempost2=  concat(convert(nvarchar,case when @mes in(1,2,3,4) then right(@añoactual,2) else right(@añoactual,2)+1 end),
					   convert(varchar,case when @mes  in(11,12,1) then 3
						     when  @mes in(2,3,4) then 4
							 when  @mes in(5,6,7) then 1
							 when  @mes in(8,9,10) then 2 end   ))



select p.Subfamilia,sum(case when [Tipo movimiento]='Compra' then Cantidad end ) as Compra,
       sum(case when m.[Cód. almacén] in ('R099','R099S') then  Cantidad end  ) as ExisAlmc,
	   format(sum(case when m.[Cód. almacén] in ('R099','R099S') then  Cantidad end  )/sum(case when [Tipo movimiento]='Compra' then Cantidad end ), 'N4','en-us') [% Exis]
    
     FROM [Mov. producto] as m  join [PRODUCTO R]  as p  on m.[Nº producto]=p.Nº
	where p.Subfamilia in(  @tempant3,
							   @tempant2,
							   @tempant1,
							  @tempact,
							  @tempost1,
							   @tempost2)
		group by p.Subfamilia