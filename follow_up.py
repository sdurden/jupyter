import prod_fup
import vend_fup
import ventas_fup
import Env_mail

import pandas as pd
import ftplib
import datetime

texto = "No se pudo enviar los datos hacia follow up"

ahora= datetime.datetime.now() + datetime.timedelta(days=-1)
AA=ahora.strftime("%Y")
MM=ahora.strftime("%m")
DD=ahora.strftime("%d")

fin = ahora + datetime.timedelta(days=-30)
aa=fin.strftime("%Y")
mm=fin.strftime("%m")
dd=fin.strftime("%d")


prod_fup.result.to_csv(r'.\Datos\productos.txt',index=None,header=True,sep='|',encoding='utf-8-sig')
vend_fup.result.to_csv(r'.\Datos\vendedores.txt',index=None,header=True,sep='|',encoding='utf-8-sig')
ventas_fup.result.to_csv(r'.\Datos\ventas-detalle{}{}{}.txt'.format(AA,MM,DD),index=None,header=True,sep='|',encoding='utf-8-sig')

## Transferencia de archivos al servidor
try:
    session = ftplib.FTP('ftp.fupbi.com','followup_habers','M2I3ZTJkMDMwY')
    session.cwd('/upload/')


    productos = open(r'.\Datos\productos.txt','rb')
    session.storbinary('STOR '+'productos.txt',productos)
    productos.close()

    print('Productos guardados')

    vendedores = open(r'.\Datos\vendedores.txt','rb')
    session.storbinary('STOR '+'vendedores.txt',vendedores)
    vendedores.close()

    print('Vendedores guardados')

    ventas = open(r'.\Datos\ventas-detalle{}{}{}_{}{}{}.txt'.format(AA,MM,DD),'rb')
    session.storbinary('STOR '+'ventas-detalle{}{}{}_{}{}{}.txt'.format(AA,MM,DD),ventas)
    ventas.close()

    print('Ventas guardadas')

    session.quit()
except:
    Env_mail.envia_mail(texto)